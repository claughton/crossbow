# Project Crossbow

**Project Crossbow** provides a set of tools designed to make it easier for computational scientists to use
cloud computing resources such as [Amazon Web Services](aws.amazon.com) and [Google Cloud Platform](cloud.google.com).

[Crossqueue](https://bitbucket.org/claughton/crossqueue) provides a batch queue style mechanism to send individual compute jobs to cloud resources:


    #!/bin/bash
	# Example GROMACS MD job
	#XQ stage protein.tpr
	#XQ instance-type c5.xlarge
	
	mdrun -deffnm protein
	
-------

The combination of [Crosscore](https://bitbucket.org/claughton/crosscore) and [Crossflow](https://bitbucket.org/claughton/crossflow) provides a Pythonic way to write and execute workflows on cloud clusters:


    ...
	# run 3 replicate MD simulations:
	restart_coordinates, logs = client.map(mdrun, [tprfile1, tprfile2, tprfile3])
	...
	
----------

Authors:

* Christian Suess
* Charlie Laughton (charles.laughton@nottingham.ac.uk)

Acknowledgements:

**Project Crossbow** is funded by [EPSRC](www.epsrc.ac.uk) grant [EP/P011993/1](https://gow.epsrc.ukri.org/NGBOViewGrant.aspx?GrantRef=EP/P011993/1)
